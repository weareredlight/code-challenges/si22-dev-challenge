# RedLight Summer Internship 2022 - Dev Challenge

![banner](/assets/banner.jpg)

# 

In this challenge we expect you to **implement** a web platform to manage "O MOELAS" bar stock 🍷.

"O MOELAS" it's a bar with a cozy atmosphere that has been working with the academic community since 1974. It's known for its extensive list of shots and cocktails with inviting prices. 

The aim of this challenge is to build a simple web platform that helps the user manage the bar.

#

### Goals

This web platform should allow the user to:

- Create new drinks
- List existing drinks
- Show an existing drink
- Update existing drinks
- Delete an existing drink
- Search for drinks

- Create new cocktails
- List existing cocktails
- Show an existing cocktail
- Update existing cocktails
- Delete an existing cocktail
- Search for cocktails
- Sell cocktails
  - Take note that whenever a cocktail is sold you must update the stock of drinks

### Phases

##### The frontend

On the frontend phase we want to see web pages were you can complete the goals established before. That is, create new drinks, list and search drinks, update or delete drinks, create new cocktails, list and search cocktails and update or delete cocktails.

Feel free to use any CSS frameworks like **Tailwind**, **Bootstrap**, or any similar one if your are familiar with it. If you want a challenge you can also try to finish this step using any web framework such as **React**, **Angular** or **VueJS**.

##### The backend

For the backend you should develop a server that responds to the frontend requests and integrates with a database that stores the information about the drinks and cocktails.

Here you're also free to use any backend technology you're familiar with, be it **Ruby on Rails**, **Django**, **ExpressJS**, or any other of your choosing. For database technologies you can achieve this either using relational databases such as **PostgreSQL** and **MySQL** or by using non-relational databases such as **MongoDB**.

##### Some extras

Once the application allows the user to perform the main goals, you can also develop the following extras:

- Validate that the backend can only receive the parameters you want it to receive, no more, no less.
- Validate that a cocktail cannot be sold if any of if its drinks runs out.
- Create validations for the form fields.
- Upload a photo for a drink.
- Add an option to create multiple drinks at once.

##### Tips

Take advantage of your strengths. If you feel that the backend is not going so well then focus more on the frontend and vice versa.

#

### Notes

A drink is composed of:

- name
- quantity
- image (optional)

Example:

- name: Tequila
- quantity: 10

A cocktail is composed of:

- name
- two or more drinks

Example:
- name: Gin Tonic
- drinks: 2 x Tonic Water, 1 x Gin

#

### Delivery
When you're done, you should fork this repository and upload your work there to share it with us or you can simply send everything in a .zip folder or a WeTransfer link.
Please try to share your process going through the steps you took to reach your final version.

